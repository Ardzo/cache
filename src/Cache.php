<?php
/**
* Library for work with cache
* 
* @version 1.1
* @copyright Copyright (C) 2010 - 2018 Richter
* @author Richter (richter@wpdom.com)
* @link http://wpdom.com
*/

namespace Ardzo\Cache;

class Cache
{
    private $cache_folder;

    /** Makes an object
    * @param string $cache_folder folder for all cache
    * @param string $section folder for section, can be subfolders: folder/subfolder1
    * @param string $id some ID of cache element
    * @param int $lifetime in seconds
    */
    function __construct($cache_folder, $section = '', $id = '', $lifetime = 30, $check_user = FALSE, $check_user_group = FALSE)
    {
        if (substr($cache_folder, -1) != '/') $cache_folder .= '/';
        $this->cache_folder = $cache_folder;

        if ($section!='' && substr($section, -1) != '/') $section .= '/';
        $this->section  = $section;

        $this->id       = md5($id.($check_user?$_SESSION[pers_set][id]:'').($check_user_group?$_SESSION[pers_set][ug]:''));
        $this->lifetime = $lifetime;
    }

    /**
    * Checks for actuality of cache
    * @return boolean TRUE if cache is found and lifetime is not expired
    */
    function cacheExists()
    {
        if (file_exists($this->cache_folder.$this->section.$this->id) && filemtime($this->cache_folder.$this->section.$this->id)>=time()-$this->lifetime) return TRUE;
        else return FALSE;
    }

    /** Checks for actuality of cache and start a caching otherwise */
    function startCache()
    {
        if ($this->cacheExists()) {
            echo file_get_contents($this->cache_folder.$this->section.$this->id);
        } else {
            ob_start();
            return TRUE;
        }
    }

    /** Shows a caching data and stores it into cache */
    function saveCache()
    {
        $tmp = ob_get_contents();
        ob_end_clean();
        echo $tmp;

        if (!is_dir($this->cache_folder)) // Folder for all cache
            mkdir($this->cache_folder);
        if (!is_dir($this->cache_folder.$this->section)) // Folder for section of cache
            mkdir($this->cache_folder.$this->section);
        file_put_contents($this->cache_folder.$this->section.$this->id, $tmp, LOCK_EX);
    }

    /** Clears a cache
    * @param int $type 1 - clear a section, 2 - clear an element, other - clear all cache
    * @return void
    */
    function clearCache($type = '')
    {
        switch ($type) {
            default:
                \Ardzo\deleteFiles($this->cache_folder, FALSE);
                break;
            case 1:
                \Ardzo\deleteFiles($this->cache_folder.$this->section, FALSE);
                break;
            case 2:
                @unlink($this->cache_folder.$this->section.$this->id);
                break;
        }
    }
}
?>
