<?php
use Ardzo\Cache\Cache;

require 'Cache.php';
require 'lib_ouf.php';
// or if used a composer:
//require '/vendor/autoload.php';

$co = new Cache('cache', '', 'block_1', 20);
if ($co->startCache()) {
    echo '
	First block: instead of "echo" here can be placed PHP code that will write any data.
	<hr />
    ';
    $co->saveCache();
}

echo 'Non-cacheable content';

$co = new Cache('cache', 'subsection1', 'block_2', 50);
if ($co->startCache()) {
    echo '
	<hr />
	Second block: instead of "echo" here can be placed PHP code that will write any data.
    ';
    $co->saveCache();
}

//$co->clearCache(1);
?>
