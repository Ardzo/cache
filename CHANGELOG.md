# Version history

## **1.1** 2018-10-16
* Class and functions is renamed
* Namespace is added
* A parameter is added in constructor
* Function `"clearCache()"` is changed
* No more dependency from a SBS "Web-Pro"

## **1.0** 2010-10-03
* This is first release
