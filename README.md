# Library for caching of HTML pages

## Introduction
This is a class for caching of HTML content.

## Requirements
* PHP 5.1+
* Windows or Unix
* library "Often Used Functions" V1.14 (http://ardzo.com/ru/soft_various.php) (automatically loads with composer)

## Using
Copy "Cache.php" from `"src"` and required libraries onto server
and use it in your script.  
Or use Composer - class will loads automatically.  
See example code in folder `"example"`.  
Folder for cache must have permission to write.
